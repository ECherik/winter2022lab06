public class Board{
		private Die dieOne;
		private Die dieTwo;
		private boolean[] closedTiles;
		
		public Board(){
			this.dieOne = new Die();
			this.dieTwo = new Die();
			this.closedTiles = new boolean[12];
		}
		
		public String toString(){
			String tiles = "";
			for(int i = 0; i < closedTiles.length; i++){
				if(closedTiles[i]){
					tiles = tiles + "X ";
				}else{
					tiles = tiles + (i+1) + " ";
				}
			}
			return tiles;
		}
		
		public boolean playATurn(){
			boolean shut=false;
			dieOne.roll();
			dieTwo.roll();
			int sum = dieOne.getPips() + dieTwo.getPips();
			
			System.out.println(this.dieOne.toString());
			System.out.println(this.dieTwo.toString());
			
			for(int i = 0; i < closedTiles.length; i++){
				if(sum-1 == i){
					if(closedTiles[i]){
						System.out.println("This position is already shut");
						shut = true;
					}
					else{
						System.out.println("Closing tile: " + (i+1));
						closedTiles[i] = true;
						shut = false;
					}
				}
			}
			return shut;
			
		}
}