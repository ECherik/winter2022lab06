import java.util.Random;
public class Die{
	private int pips;
	private Random random;
	
	public Die(){
		this.pips = 1;
		this.random = new Random();
	}
	
	public int getPips(){
		return this.pips;
	}
	
	public void roll(){
		this.pips = 1 + random.nextInt(6);
	}
	
	public String toString(){
		return "Pips: " + this.pips;
		}
}