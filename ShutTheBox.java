public class ShutTheBox{
	public static void main(String[]args){
		Board board = new Board();
		boolean gameOver = false;
	
		System.out.println("Welcome to ShutTheBox!");
		
		do{
			System.out.println("Player 1's turn");
			System.out.println(board.toString());
			
			if(board.playATurn()){
				System.out.println("Player 2 wins!");
				gameOver = true;
			}else{
				System.out.println("Player 2's turn");
				System.out.println(board.toString());
			if(board.playATurn()){
				System.out.println("Player 1 wins!");
				gameOver = true;
			}
			}
		}while(gameOver == false);
		
	}
}